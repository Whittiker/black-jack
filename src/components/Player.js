import React, { Component } from 'react';
import Hand from './Hand';
import Deck from './Deck';
// import '../style/css/Player.css';

class Player extends Component {
  constructor(){
    super()
    this.state = {
      cardsInHand: [{}],
      visibleCount:0,
      score:0,
      haveAce: 0,
      bust:false
    }
    this.drawCard = this.drawCard.bind(this);
    this.createHand = this.createHand.bind(this);  
  }
  
  createHand = () => {
    let randomHand = this.props.randomLists.map(ranNum => Deck[ranNum])
                                .map(card => {return{value:card.value, suit:card.suit, inhand:false, worth:card.worth} }) // TODO: this was added becuase cards previously in hand had remaining true inHand values
    this.setState({
        cardsInHand: randomHand,
        visibleCount:0,
        score:0,
        haveAce: 0,
        bust:false
      })
      this.drawCard()
      this.drawCard()
  }

  drawCard = () => {
    // draws card and adds it to score
    this.setState(prevState =>{
      if (prevState.visibleCount > 4){
        return prevState
      }
      let drawnCard = prevState.cardsInHand[prevState.visibleCount]
      drawnCard.inhand = true  // TODO: this need to be changed so that it doesnt directly change the object

      let hasAce = drawnCard.value === "A" || prevState.haveAce === 1 ? 1 : 0
      let newScore = prevState.score + drawnCard.worth
      return{
        cardsInHand: prevState.cardsInHand,
        visibleCount: prevState.visibleCount + 1,
        score: newScore,
        scoreWithAce: hasAce ? (newScore + 11) : newScore,
        haveAce: hasAce,
        bust: prevState.score + drawnCard.worth > 21
      }
    })
  }

  render() {
    return (
      <div className = "blackjackContainer">
           <button onClick={this.drawCard}>Draw Card</button>
           <button onClick={this.createHand}>Create Hand</button>
           {<Hand cards={this.state.cardsInHand} score={this.state.score} haveAce={this.state.haveAce} bust={this.state.bust} scoreWithAce ={this.state.scoreWithAce} />}

      </div>
      )
  }
}

export default Player;
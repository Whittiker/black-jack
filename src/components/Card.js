

import React from 'react';
// import '../style/css/Card.css';

const Card = (props) => {
    return (
      <div className ={'Card ' + props.card.suit}>
            <p>{props.card.value}</p> 
      </div>
      
    );
}

export default Card;
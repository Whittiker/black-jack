// TODO: Chanage all random id lists to randomislist
import React, { Component } from 'react';
import '../style/css/App.css';
import Player from './Player';
import Dealer from './Dealer';


class App extends Component {
  constructor(){
    super()
    this.state = {
      randomLists: null,
      dealerRandomList: null
    }
  this.playerNumHandler = this.playerNumHandler.bind(this)
  }

  playerNumHandler = (event) => {
    let randomIdList = this.generateRandomIdLists(event.target.value)
    this.setState({
      randomLists: randomIdList.slice(1,randomIdList.length-1),
      dealerRandomList: randomIdList[0]
      })
  }
  
  generateRandomIdLists = (numPlayers) => {
    let randomIds = []
    let uniqueRandomLists = []
    while(randomIds.length < (numPlayers)*5){
      let ranNum = Math.floor(Math.random() * 51)
      if (randomIds.indexOf(ranNum) === -1){
        randomIds.push(ranNum)
      }
    }
    for(let i=0; i<(numPlayers)*5; i+=5){
      uniqueRandomLists.push(randomIds.slice(i, i+5))
    }
    return uniqueRandomLists
  }

  render() {
    return (



      
      <div className="app">

        <div className="app__row"></div>
        <div className="app__row" id="app__row__middle">
          <div className="app__column"></div>
          <section className="app__column" id="app__column__middle"> 

             <label> 
                How many Players
                <select onChange={this.playerNumHandler}>
                  <option value={2}>1</option>
                  <option value={3}>2</option>
                  <option value={4}>3</option>
                  <option value={5}>4</option>
                </select>
              </label>
            {this.state.randomLists !== null && this.state.randomLists.map((randomLists, index) => 
              <Player randomLists={randomLists} key={index}/>)}
          
            {this.state.dealerRandomList !== null && <Dealer dealerRandomList={this.state.dealerRandomList}/>}

          </section>
          <div className="app__column"></div>
        </div>
        <div className="app__row"></div>
      </div>
    );
  }
}

export default App;



import React from 'react';
import Card from './Card';
// import '../style/css/Hand.css';

const Hand = (props) => {
  const cardComponents = props.cards
    return (
      <div className = 'Hand'>
       <h1>{props.bust && "Bust: "} Score: {props.score} {props.haveAce === 1 && ", Or Score: " + (props.scoreWithAce)} </h1>     
        {cardComponents.map((card, index) => card.inhand && <Card key={index} card={card}/>)}
      </div>
    );
};

export default Hand;
